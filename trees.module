<?php

/**
 * @file
 * Allows users to structure nodes of a site in a hierarchy or outline.
 */

/**
 * Implementation of hook_init(). Add's the trees module's CSS + JS.
 */
function trees_init() {
  drupal_add_css(drupal_get_path('module', 'trees') .'/trees.css');
  if(arg(0) == 'node' && is_numeric(arg(1))) {
    global $base_path;
    if (arg(0) == 'node' && is_numeric(arg(1))) {
      drupal_add_js('var node_nid = '.arg(1).';', 'inline');
    }
    drupal_add_js(drupal_get_path('module', 'trees'). '/trees_ajax.js');
    $settings = array('ajax_path' => url('ajax/trees'));
    drupal_add_js(array('trees' => $settings), "setting");
  }
}

/**
 * Implementation of hook_views_api()
 */

function trees_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'trees'),
  );
}

/**
 * Implementation of hook_theme()
 */
function trees_theme() {
  return array(
    'trees_admin_table' => array(
      'arguments' => array('form' => NULL),
    ),
    'trees_leaves' => array(
      'arguments' => array('leaves' => array()),
      'file' => 'theme.inc',
    ),
    'trees_parents' => array(
      'arguments' => array('parents' => array()),
      'file' => 'theme.inc',
    ),
    'trees_leave' => array(
      'arguments' => array('leave' => array()),
      'file' => 'theme.inc',
    ),
  );
}

/**
 * Implementation of hook_perm().
 */
function trees_perm() {
  return array('add content to trees', 'administer trees outlines', 'create new trees', 'view outline tab', 'create new trees on node');
}

/**
 * Implementation of hook_menu().
 */
function trees_menu() {
  $items['admin/content/trees'] = array(
    'title' => 'trees',
    'description' => "Manage your site's trees outlines.",
    'page callback' => 'trees_admin_overview',
    'access arguments' => array('administer trees outlines'),
    'file' => 'trees.admin.inc',
  );
  $items['admin/content/trees/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/content/trees/settings'] = array(
    'title' => 'Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('trees_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 8,
    'file' => 'trees.admin.inc',
  );
  $items['admin/content/trees/%node'] = array(
    'title' => 'Re-order trees pages and change titles',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('trees_admin_edit', 3),
    'access callback' => '_trees_outline_access',
    'access arguments' => array(3),
    'type' => MENU_CALLBACK,
    'file' => 'trees.admin.inc',
  );
  $items['trees'] = array(
    'title' => 'trees',
    'page callback' => 'trees_render',
    'access arguments' => array('access content'),
    'type' => MENU_SUGGESTED_ITEM,
    'file' => 'trees.pages.inc',
  );
  $items['node/%node/outline'] = array(
    'title' => 'Outline',
    'page callback' => 'trees_outline',
    'page arguments' => array(1),
    'access callback' => 'trees_access_outline',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
    'file' => 'trees.pages.inc',
  );
  $items['node/%node/outline/remove/%'] = array(
    'title' => 'Remove from outline',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('trees_remove_form', 1, 4),
    'access callback' => '_trees_outline_remove_access',
    'access arguments' => array(1),
    'type' => MENU_CALLBACK,
    'file' => 'trees.pages.inc',
  );
  $items['ajax/trees'] = array(
    'page callback' => 'trees_ajax_update',
    'file' => 'trees_blocks.inc',
    'access callback' => TRUE
  );
  return $items;
}

/**
 * Access callback for outline
 */
function trees_access_outline ($node) {
  return (trees_type_is_allowed($node->type) && user_access('view outline tab'));
}

/**
 * Menu item access callback - determine if the outline tab is accessible.
 */
function _trees_outline_access($node) {
  return user_access('administer trees outlines') && node_access('view', $node);
}

/**
 * Menu item access callback - determine if the user can remove nodes from the outline.
 */
function _trees_outline_remove_access($node) {
  return isset($node->trees) && ($node->trees['bid'] != $node->nid) && _trees_outline_access($node);
}

/**
 * Returns an array of all trees.
 *
 * This list may be used for generating a list of all the trees, or for building
 * the options for a form select.
 */
function trees_get_trees() {
  static $all_trees;

  if (!isset($all_trees)) {
    $all_trees = array();
    $result = db_query("SELECT bid, nid FROM {trees}");
    $nids = array();
    while ($trees = db_fetch_array($result)) {
      $nids[] = $trees['nid'];
    }
  
    if ($nids) {
      $result2 = db_query(db_rewrite_sql("SELECT n.type, n.title, b.*, ml.* FROM {trees} b INNER JOIN {node} n on b.nid = n.nid INNER JOIN {menu_links} ml ON b.mlid = ml.mlid WHERE n.nid IN (". implode(',', $nids) .") AND n.status = 1 AND plid = 0 ORDER BY ml.weight, ml.link_title"));

      while ($link = db_fetch_array($result2)) {
        $link['href'] = $link['link_path'];
        $link['options'] = unserialize($link['options']);
        $all_trees[$link['nid']] = $link;
      }
    }
  }
  return $all_trees;
}

/**
 * Build the parent selection form element for the node form or outline tab
 *
 * This function is also called when generating a new set of options during the
 * AJAX callback, so an array is returned that can be used to replace an existing
 * form element.
 */
 //new
function _trees_parent_select($trees_link) {
  if (variable_get('menu_override_parent_selector', FALSE)) {
    return array();
  }
  // Offer a message or a drop-down to choose a different parent page.
  $form = array(
    '#type' => 'hidden',
    '#value' => -1,
    '#prefix' => '<div id="edit-trees-plid-wrapper">',
    '#suffix' => '</div>',
  );

  $nid = db_result(db_query("SELECT t.nid FROM {trees} t WHERE bid = %d", $trees_link['bid']));

  if ($nid == $trees_link['nid'] || !$nid) {
    // This is a tree - at the top level.
    if ($trees_link['original_bid'] == $trees_link['bid']) {
      $form['#prefix'] .= '<em>'. t('This is the top-level page in this tree.') .'</em>';
    }
    else {
      $form['#prefix'] .= '<em>'. t('This will be the top-level page in this tree.') .'</em>';
    }
  }
  elseif (!$trees_link['bid'] && $trees_link['bid'] == 'new') {
    $form['#prefix'] .= '<em>'. t('No trees selected.') .'</em>';
  }
  else {
    $exclude[] =  $trees_link['mlid'];
    $form = array(
      '#type' => 'select',
      '#title' => t('Parent item'),
      '#default_value' => $trees_link['plid'],
      '#description' => t('The parent page in the trees. The maximum depth for a trees and all child pages is !maxdepth. Some pages in the selected trees may not be available as parents if selecting them would exceed this limit.', array('!maxdepth' => MENU_MAX_DEPTH)),
      '#options' => trees_toc($trees_link['bid'], $exclude, $trees_link['parent_depth_limit'], 'product'),
      '#attributes' => array('class' => 'trees-title-select'),
    );
  }
  return $form;
}

/**
 * Implementation of hook_form_alter(). Adds the trees fieldset to the node form.
 *
 * @see trees_pick_trees_submit()
 * @see trees_submit()
 */
function trees_form_alter(&$form, $form_state, $form_id) {
  $node = $form['#node'];
  if (trees_type_is_allowed($node->type) && isset($form['type']) && isset($form['#node']) && $form['type']['#value'] .'_node_form' == $form_id) {
    // Add elements to the node form


    $access = user_access('administer trees outlines');
    if ($node->trees) {
      foreach ($node->trees as $bid => $trees2) {
        if (!$access) {
          if (user_access('add content to trees') && ((!empty($node->trees[$bid]['mlid']) && !empty($node->nid)) || trees_type_is_allowed($node->type))) {
            // Already in the trees hierarchy or this node type is allowed
            $access = TRUE;
          }
        }

        if ($access && trees_type_is_allowed($node->type)) {
          _trees_add_form_elements($form, $node);
        }
      }
    }
  }
}

/**
 * Build the common elements of the trees form for the node and outline forms.
 */
function _trees_add_form_elements(&$form, $node, $bid = NULL) {
  // Need this for AJAX.
  $form['#cache'] = TRUE;
  $form['trees']['#tree'] = true;

  if ($bid) {
    $trees[$bid] = $node->trees[$bid];
    $bbid = true;
    $form['trees'][$bid] = array(
      '#type' => 'fieldset',
      '#title' => ($bid == 'new') ? t('Create new trees') : t('Tree outline : @title', array('@title' =>  _trees_get_root_name($bid))) ,
      '#weight' => 10,
      '#tree' => TRUE,
      '#attributes' => array('class' => 'trees-outline-form'),
    );
  }
  else {
    $trees = $node->trees;
    $form['trees'] = array(
      '#type' => 'fieldset',
      '#title' => ($bid == 'new') ? t('Create new trees') : t('Tree outline : @title', array('@title' =>  _trees_get_root_name($bid))) ,
      '#weight' => 10,
      '#tree' => TRUE,
      '#attributes' => array('class' => 'trees-outline-form'),
    );
  }



  foreach ($trees as $bid => $tree) {
    if (is_numeric($bid) || $bid == 'new') {
      drupal_add_js("if (Drupal.jsEnabled) { $(document).ready(function() { $('#edit-trees-pick-trees-$bid').css('display', 'none'); }); }", 'inline');

      if (!$bbid) {
        $form['trees'][$bid] = array(
          '#type' => 'fieldset',
          '#title' => ($bid == 'new') ? t('Create new trees') : t('Tree outline : @title', array('@title' =>  _trees_get_root_name($bid))) ,
          '#weight' => 10,
          '#collapsed' => (arg(1) == 'edit') ? TRUE : FALSE,
          '#collapsible' => TRUE,
        );
      }

      foreach (array('menu_name', 'mlid', 'nid', 'router_path', 'has_children', 'options', 'module', 'original_bid', 'parent_depth_limit') as $key) {
        $form['trees'][$bid][$key] = array(
          '#type' => 'value',
          '#value' => $node->trees[$bid][$key],
        );
      }

      //when a new form needs to be build set bid and nid equal the correct form will be rendered by an ajax call
      if ($bid == 'new') {
        $node->trees[$bid]['bid'] = $bid;
        $node->trees[$bid]['nid'] = $bid;
      }
      else {
        $node->trees[$bid]['bid'] = $bid;
        $node->trees[$bid]['parent_depth_limit'] = 8;
      }

      $form['trees'][$bid]['plid'] = _trees_parent_select($node->trees[$bid]);

      $form['trees'][$bid]['plid']['#options'][0] = '<none>';

      $form['trees'][$bid]['weight'] = array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => $node->trees[$bid]['weight'],
        '#delta' => 15,
        '#weight' => 5,
        '#access' => false,
        '#description' => t('Pages at a given level are ordered first by weight and then by title.'),
      );
      $options = array();
      $nid = isset($node->nid) ? $node->nid : 'new';

      if (user_access('create new trees') && ($nid == 'new' || ($nid != $node->trees[$bid]['original_bid']))) {
        // The node can become a new trees, if it is not one already.
        $options = array($nid => '<'. t('create a new trees') .'>') + $options;
      }
      if (!$node->trees[$bid]['mlid']) {
        // The node is not currently in a the hierarchy.
        $options = array(0 => '<'. t('none') .'>') + $options;
      }

      // Add a drop-down to select the destination trees.

      $form['trees'][$bid]['bid'] = array(
        '#type' => 'select',
        '#title' => t('trees'),
        '#default_value' => ($bid != 'new') ? $node->trees[$bid]['bid'] : '<none>',
        '#options' => $options,
        '#access' => (bool)$options,
        '#description' => ($bid != 'new') ? t('Your page will be a part of the selected trees.') : '',
        '#weight' => -5,
        '#access' => ($bid == 'new') ? true : false,
        '#attributes' => array('class' => 'trees-title-select'),
      );

    }
  }

  $form['#validate'][] = 'trees_validate_outline';
}

/**
 * Common helper function to handles additions and updates to the trees outline.
 *
 * Performs all additions and updates to the trees outline through node addition,
 * node editing, node deletion, or the outline tab.
 */
function _trees_update_outline(&$node) {
  if (empty($node->trees['bid'])) {
    return FALSE;
  }
  $new = empty($node->trees['mlid']);

  $node->trees['link_path'] = 'node/'. $node->nid;
  $node->trees['link_title'] = $node->title;
  $node->trees['parent_mismatch'] = FALSE; // The normal case.

  if ($node->trees['bid'] == $node->nid) {
    $node->trees['plid'] = 0;
    $node->trees['menu_name'] = trees_menu_name($node->nid);
  }
  else {
    // Check in case the parent is not is this trees; the trees takes precedence.
    if (!empty($node->trees['plid'])) {
      $parent = db_fetch_array(db_query("SELECT * FROM {trees} WHERE mlid = %d", $node->trees['plid']));
    }
    if (empty($node->trees['plid']) || !$parent || $parent['bid'] != $node->trees['bid']) {
      $node->trees['plid'] = db_result(db_query("SELECT mlid FROM {trees} WHERE nid = %d AND bid = %d", $node->trees['bid'], $node->trees['bid']));
      $node->trees['parent_mismatch'] = TRUE; // Likely when JS is disabled.
    }
  }

  if (menu_link_save($node->trees)) {
    if ($new) {
      // Insert new.
      db_query("INSERT INTO {trees} (nid, mlid, bid) VALUES (%d, %d, %d)", $node->nid, $node->trees['mlid'], $node->trees['bid']);
    }
    else {
      if ($node->trees['bid'] != db_result(db_query("SELECT bid FROM {trees} WHERE nid = %d", $node->nid))) {
        // Update the bid for this page and all children.
        trees_update_bid($node->trees);
      }
    }
    return TRUE;
  }
  // Failed to save the menu link
  return FALSE;
}

/**
 * Update the bid for a page and its children when it is moved to a new trees.
 *
 * @param $trees_link
 *   A fully loaded menu link that is part of the trees hierarchy.
 */
function trees_update_bid($trees_link) {

  for ($i = 1; $i <= MENU_MAX_DEPTH && $trees_link["p$i"]; $i++) {
    $match[] = "p$i = %d";
    $args[] = $trees_link["p$i"];
  }
  $result = db_query("SELECT mlid FROM {menu_links} WHERE ". implode(' AND ', $match), $args);

  $mlids = array();
  while ($a = db_fetch_array($result)) {
    $mlids[] = $a['mlid'];
  }
  if ($mlids) {
    db_query("UPDATE {trees} SET bid = %d WHERE mlid IN (". implode(',', $mlids) .")", $trees_link['bid']);
  }
}

/**
 * Generate the corresponding menu name from a trees ID.
 */
function trees_menu_name($bid) {
  return 'trees-toc-'. $bid;
}

/**
 * Implementation of hook_nodeapi().
 *
 * Appends trees navigation to all nodes in the trees, and handles trees outline
 * insertions and updates via the node form.
 */
 //New function
function trees_nodeapi(&$node, $op, $teaser, $page) {
  if (trees_type_is_allowed($node->type)) {


    switch ($op) {
      case 'load':
        // Note - we cannot use trees_link_load() because it will call node_load()
        $result = db_query('SELECT * FROM {trees} b INNER JOIN {menu_links} ml ON b.mlid = ml.mlid WHERE b.nid = %d', $node->nid);

        while ($row = db_fetch_array($result)) {
          $info['trees'][$row['bid']] = $row;
          if ($info['trees'][$row['bid']]) {
            $info['trees'][$row['bid']]['href'] = $info['trees'][$row['bid']]['link_path'];
            $info['trees'][$row['bid']]['title'] = $info['trees'][$row['bid']]['link_title'];
            $info['trees'][$row['bid']]['options'] = unserialize($info['trees'][$row['bid']]['options']);
          }
        }
        return $info;
        break;
      case 'presave':
        if (empty($node->nid)) {
          $bids = _trees_get_treesbids();
          $bids['new'] = 'new';
          foreach ($bids as $bid) {
            $node->trees[$bid]['mlid'] = NULL;
          }
        }
        break;
      case 'insert':
      case 'update':
        $trees = $node->trees;
        if (!empty($trees)) {
          foreach ($trees as $bid => $tree) {
            if (is_array($tree) && !empty($tree['bid'])) {
              $node->trees = $tree;
              if ($tree['bid'] == 'new') {
                // New nodes that are their own trees.
                $node->trees['bid'] = $node->nid;
              }
              $node->trees['nid'] = $node->nid;
              $node->trees['menu_name'] = trees_menu_name($tree['bid']);
              _trees_update_outline($node);
              $tree = $node->trees;
              $node->trees = $trees;
              $node->trees[$bid] = $tree;
            }
            elseif ($tree['plid'] != -1) {
              menu_link_delete($tree['mlid']);
              db_query('DELETE FROM {trees} WHERE nid = %d AND mlid = %d', $node->nid, $tree['mlid']);
            }
          }
        }
        break;
      case 'delete':
        if ($node->trees) {
          foreach ($node->trees as $bid => $tree) {
            if (!empty($tree['bid'])) {
              if ($node->nid == $tree['bid']) {
                // Handle deletion of a top-level post.
                $result = db_query("SELECT b.nid FROM {menu_links} ml INNER JOIN {trees} b on b.mlid = ml.mlid WHERE ml.plid = %d", $tree['mlid']);
                while ($child = db_fetch_array($result)) {
                  $child_node = node_load($child['nid']);
                  $trees2 = $child_node->trees;
                  foreach ($child_node->trees as $bid2 => $tree2) {
                    $trees['bid'] = $child_node->nid;

                    $child_node->trees = $tree2;
                    _trees_update_outline($child_node);
                    $tree2 = $child_node->trees;
                    $child_node->trees = $trees2;
                    $child_node->trees[$bid2] = $tree2;
                    $trees2 = $child_node->trees;
                  }
                }
              }
              dsm($tree['mlid']);
              menu_link_delete($tree['mlid']);
              db_query('DELETE FROM {trees} WHERE mlid = %d', $tree['mlid']);
            }
          }
        }
        break;
      case 'prepare':
        $bids = _trees_get_treesbids();
        if (!trees_check_root() && user_access('create new trees on node')) {
          $bids['new'] = 'new';
        }
        if (!empty($bids)) {
          foreach ($bids as $bid) {
            if (empty($node->trees[$bid]) && (user_access('add content to trees') || user_access('administer trees outlines'))) {
              $node->trees[$bid] = array();
              if (empty($node->nid) && isset($_GET['parent-'.$bid]) && is_numeric($_GET['parent-'.$bid])) {
                // Handle "Add child page" links:
                $parent = trees_link_load($_GET['parent-'.$bid]);//zo moet de url geformat worden
                if ($parent && $parent['access']) {
                  $node->trees[$bid]['bid'] = $parent['bid'];
                  $node->trees[$bid]['plid'] = $parent['mlid'];
                  $node->trees[$bid]['menu_name'] = $parent['menu_name'];
                }
              }
              // Set defaults.
              $node->trees[$bid] += _trees_link_defaults(!empty($node->nid) ? $node->nid : 'new');
            }
            else {
              if (isset($node->trees['bid']) && !isset($node->trees['original_bid'])) {
                $node->trees[$bid]['original_bid'] = $node->trees[$bid]['bid'];
              }
            }
            // Find the depth limit for the parent select.
            if (isset($node->trees['bid']) && !isset($node->trees['parent_depth_limit'])) {
              $node->trees[$bid]['parent_depth_limit'] = _trees_parent_depth_limit($node->trees[$bid]);
            }
          }
        }
        break;
    }
  }
}

/**
 * Find the depth limit for items in the parent select.
 */
function _trees_parent_depth_limit($trees_link) {
  return MENU_MAX_DEPTH - 1 - (($trees_link['mlid'] && $trees_link['has_children']) ? menu_link_children_relative_depth($trees_link) : 0);
}

/**
 * Form altering function for the confirm form for a single node deletion.
 */
function trees_form_node_delete_confirm_alter(&$form, $form_state) {

  $node = node_load($form['nid']['#value']);

  if (isset($node->trees) && $node->trees['has_children']) {
    $form['trees_warning'] = array(
      '#value' => '<p>'. t('%title is part of a trees outline, and has associated child pages. If you proceed with deletion, the child pages will be relocated automatically.', array('%title' => $node->title)) .'</p>',
      '#weight' => -10,
    );
  }
}

/**
 * Return an array with default values for a trees link.
 */
function _trees_link_defaults($nid) {
  return array('original_bid' => 0, 'menu_name' => '', 'nid' => $nid, 'bid' => 0, 'router_path' => 'node/%', 'plid' => 0, 'mlid' => 0, 'has_children' => 0, 'weight' => 0, 'module' => 'trees', 'options' => array());
}

/**
 * A recursive helper function for trees_toc().
 */
function _trees_toc_recurse($trees, $indent, &$toc, $exclude, $depth_limit) {
  foreach ($trees as $data) {
    if ($data['link']['depth'] > $depth_limit) {
      // Don't iterate through any links on this level.
      break;
    }
    if (!in_array($data['link']['mlid'], $exclude)) {
      $toc[$data['link']['mlid']] = $indent .' '. truncate_utf8($data['link']['title'], 30, TRUE, TRUE);
      if ($data['below']) {
        _trees_toc_recurse($data['below'], $indent .'--', $toc, $exclude, $depth_limit);
      }
    }
  }
}

/**
 * Returns an array of trees pages in table of contents order.
 *
 * @param $bid
 *   The ID of the trees whose pages are to be listed.
 * @param $exclude
 *   Optional array of mlid values.  Any link whose mlid is in this array
 *   will be excluded (along with its children).
 * @param $depth_limit
 *   Any link deeper than this value will be excluded (along with its children).
 * @return
 *   An array of mlid, title pairs for use as options for selecting a trees page.
 */
function trees_toc($bid, $exclude = array(), $depth_limit) {

  $trees = menu_tree_all_data(trees_menu_name($bid));
  $toc = array();
  _trees_toc_recurse($trees, '', $toc, $exclude, $depth_limit);

  return $toc;
}

/**
 * Determine if a given node type is in the list of types allowed for trees.
 */
function trees_type_is_allowed($type) {
  return in_array($type, variable_get('trees_allowed_types', array('trees')));
}

/**
 * Implementation of hook_node_type().
 *
 * Update trees module's persistent variables if the machine-readable name of a
 * node type is changed.
 */
function trees_node_type($op, $type) {

  switch ($op) {
    case 'update':
      if (!empty($type->old_type) && $type->old_type != $type->type) {
        // Update the list of node types that are allowed to be added to trees.
        $allowed_types = variable_get('trees_allowed_types', array('trees'));
        $key = array_search($type->old_type, $allowed_types);
        if ($key !== FALSE) {
          $allowed_types[$type->type] = $allowed_types[$key] ? $type->type : 0;
          unset($allowed_types[$key]);
          variable_set('trees_allowed_types', $allowed_types);
        }
        // Update the setting for the "Add child page" link.
        if (variable_get('trees_child_type', 'trees') == $type->old_type) {
          variable_set('trees_child_type', $type->type);
        }
      }
      break;
  }
}

/**
 * Implementation of hook_help().
 */
function trees_help($path, $arg) {
  switch ($path) {
    case 'admin/help#trees':
      $output = '<p>'. t('The trees module is suited for creating structured, multi-page hypertexts such as site resource guides, manuals, and Frequently Asked Questions (FAQs). It permits a document to have chapters, sections, subsections, etc. Authors with suitable permissions can add pages to a collaborative trees, placing them into the existing document by adding them to a table of contents menu.') .'</p>';
      $output .= '<p>'. t('Pages in the trees hierarchy have navigation elements at the bottom of the page for moving through the text. These links lead to the previous and next pages in the trees, and to the level above the current page in the trees\'s structure. More comprehensive navigation may be provided by enabling the <em>trees navigation block</em> on the <a href="@admin-block">blocks administration page</a>.', array('@admin-block' => url('admin/build/block'))) .'</p>';
      $output .= '<p>'. t('Users can select the <em>printer-friendly version</em> link visible at the bottom of a trees page to generate a printer-friendly display of the page and all of its subsections. ') .'</p>';
      $output .= '<p>'. t("Users with the <em>administer trees outlines</em> permission can add a post of any content type to a trees, by selecting the appropriate trees while editing the post or by using the interface available on the post's <em>outline</em> tab.") .'</p>';
      $output .= '<p>'. t('Administrators can view a list of all trees on the <a href="@admin-node-trees">trees administration page</a>. The <em>Outline</em> page for each trees allows section titles to be edited or rearranged.', array('@admin-node-trees' => url('admin/content/trees'))) .'</p>';
      $output .= '<p>'. t('For more information, see the online handtrees entry for <a href="@trees">trees module</a>.', array('@trees' => 'http://drupal.org/handtrees/modules/trees/')) .'</p>';
      return $output;
    case 'admin/content/trees':
      return '<p>'. t('The trees module offers a means to organize a collection of related posts, collectively known as a trees. When viewed, these posts automatically display links to adjacent trees pages, providing a simple navigation system for creating and reviewing structured content.') .'</p>';
    case 'node/%/outline':
      return '<p>'. t('The outline feature allows you to include posts in the <a href="@trees">trees hierarchy</a>, as well as move them within the hierarchy or to <a href="@trees-admin">reorder an entire trees</a>.', array('@trees' => url('trees'), '@trees-admin' => url('admin/content/trees'))) .'</p>';
  }
}

/**
 * Like menu_link_load(), but adds additional data from the {trees} table.
 *
 * Do not call when loading a node, since this function may call node_load().
 */
function trees_link_load($mlid) {
  if ($item = db_fetch_array(db_query("SELECT * FROM {menu_links} ml INNER JOIN {trees} b ON b.mlid = ml.mlid LEFT JOIN {menu_router} m ON m.path = ml.router_path WHERE ml.mlid = %d", $mlid))) {
    _menu_link_translate($item);
    return $item;
  }
  return FALSE;
}

/**
 * Get the data representing a subtrees of the trees hierarchy.
 *
 * The root of the subtrees will be the link passed as a parameter, so the
 * returned trees will contain this item and all its descendents in the menu trees.
 *
 * @param $item
 *   A fully loaded menu link.
 * @return
 *   An subtrees of menu links in an array, in the order they should be rendered.
 */
function trees_menu_subtrees_data($item) {
  static $trees = array();

  // Generate a cache ID (cid) specific for this $menu_name and $item.
  $cid = 'links:'. $item['menu_name'] .':subtrees-cid:'. $item['mlid'];

  if (!isset($trees[$cid])) {
    $cache = cache_get($cid, 'cache_menu');
    if ($cache && isset($cache->data)) {
      // If the cache entry exists, it will just be the cid for the actual data.
      // This avoids duplication of large amounts of data.
      $cache = cache_get($cache->data, 'cache_menu');
      if ($cache && isset($cache->data)) {
        $data = $cache->data;
      }
    }
    // If the subtrees data was not in the cache, $data will be NULL.
    if (!isset($data)) {
      $match = array("menu_name = '%s'");
      $args = array($item['menu_name']);
      $i = 1;
      while ($i <= MENU_MAX_DEPTH && $item["p$i"]) {
        $match[] = "p$i = %d";
        $args[] = $item["p$i"];
        $i++;
      }
      $sql = "
        SELECT b.*, m.load_functions, m.to_arg_functions, m.access_callback, m.access_arguments, m.page_callback, m.page_arguments, m.title, m.title_callback, m.title_arguments, m.type, ml.*
        FROM {menu_links} ml INNER JOIN {menu_router} m ON m.path = ml.router_path
        INNER JOIN {trees} b ON ml.mlid = b.mlid
        WHERE ". implode(' AND ', $match) ."
        ORDER BY p1 ASC, p2 ASC, p3 ASC, p4 ASC, p5 ASC, p6 ASC, p7 ASC, p8 ASC, p9 ASC";

      $data['trees'] = menu_tree_data(db_query($sql, $args), array(), $item['depth']);
      $data['node_links'] = array();
      menu_tree_collect_node_links($data['trees'], $data['node_links']);
      // Compute the real cid for trees subtrees data.
      $trees_cid = 'links:'. $item['menu_name'] .':subtrees-data:'. md5(serialize($data));
      // Cache the data, if it is not already in the cache.
      if (!cache_get($trees_cid, 'cache_menu')) {
        cache_set($trees_cid, $data, 'cache_menu');
      }
      // Cache the cid of the (shared) data using the menu and item-specific cid.
      cache_set($cid, $trees_cid, 'cache_menu');
    }
    // Check access for the current user to each item in the trees.
    menu_tree_check_access($data['trees'], $data['node_links']);
    $trees[$cid] = $data['trees'];
  }

  return $trees[$cid];
}

//New functions
/*
 * Helper function get bids of all trees
 */
function _trees_get_treesbids() {
  $result = db_query("SELECT DISTINCT bid FROM {trees}");
  while ($bid = db_fetch_object($result)) {
    $bids[] = $bid->bid;
  }
  return $bids;
}


/**
 * Helper function
 * Get's the name of the root node of a trees
 *
 * @bid the id of the trees
 * @return the name of the trees or false if no match is found
 */
function _trees_get_root_name($bid) {
  $result = db_result(db_query("SELECT link_path FROM {menu_links} WHERE menu_name = '%s' AND plid = 0", 'trees-toc-'.$bid));
  if ($result) {
    $path = explode('/', $result);
    $nid = $path[1];
    return db_result(db_query("SELECT title FROM {node} WHERE nid = %d", $nid));
  }
  return false;
}

/*
 * Validation function will check if an outline is selected, other wise do not save in the database
 * When an outline is allready existing delete it at the outline, it is not part of this function
 */
function trees_validate_outline($form, &$form_state) {
  foreach ($form_state['values']['trees'] as $bid => $trees) {
    if ($trees['plid'] == 0) {
      $form_state['values']['trees'][$bid]['bid'] = 0;
    }
  }
}

/*
 * Helper function, looks if a node is allready the root of another trees
 *
 * @param bid the trees id
 * @return true/false
 */
function trees_check_root() {
  $result = db_result(db_query("SELECT m.mlid FROM menu_links m INNER JOIN trees t ON t.mlid = m.mlid WHERE m.plid = %d AND m.link_path = '%s' AND m.module = '%s'", 0, arg(0).'/'.arg(1), 'trees'));

  return ($result) ? true : false;
}

/**
 * Implementation of hook_block().
 */
function trees_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':

      $blocks = array();

      $trees = trees_get_trees();
      foreach($trees as $tree) {

        $blocks[$tree['bid'] .'_siblings'] = array(
          'info' => t('@title - Current siblings', array('@title' => $tree['title'])),
          'cache' => BLOCK_CACHE_PER_PAGE | BLOCK_CACHE_PER_USER,
        );
        $blocks[$tree['bid'] .'_parents'] = array(
          'info' => t('@title - Current parents', array('@title' => $tree['title'])),
          'cache' => BLOCK_CACHE_PER_PAGE | BLOCK_CACHE_PER_USER,
        );
        $blocks[$tree['bid'] .'_children'] = array(
          'info' => t('@title - Current children', array('@title' => $tree['title'])),
          'cache' => BLOCK_CACHE_PER_PAGE | BLOCK_CACHE_PER_USER,
        );

      }

      return $blocks;
    break;

    case 'view':

      if(arg(0) !== 'node' || !is_numeric(arg(1))) {
        return;
      }
      $node = node_load(arg(1));

      include_once(dirname(__FILE__). '/trees_blocks.inc');
      list($bid, $delta) = explode('_', $delta, 2);

      switch($delta) {
        case 'parents':
          return get_node_parents_by_trees($bid, $node);
        break;

        case 'children':

          $page = 0;
          if(isset($_GET['block']) && $_GET['block'] == 'children') {
            $page = $_GET['page'];
          }

          $block = get_node_children_by_trees($bid, $node, $page);
          if(is_array($block) && !empty($block['content'])) {
            $block['content'] = '<div class="trees-content-holder">' . $block['content'] . '</div>';
          }
          return $block;

        break;

        case 'siblings':

          $page = 0;
          if(isset($_GET['block']) && $_GET['block'] == 'siblings') {
            $page = $_GET['page'];
          }

          $block = get_node_siblings_by_trees($bid, $node, $page);
          if(is_array($block) && !empty($block['content'])) {
            $block['content'] = '<div class="trees-content-holder">' . $block['content'] . '</div>';
          }
          return $block;

        break;
      }
    break;
  }
}
