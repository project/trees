<?php
/**
 * @file
 * Provide views data and handlers for trees.module
 */

/**
 * Implementation of hook_views_data()
 */
function trees_views_data() {
  // ----------------------------------------------------------------------
  // trees table

  $data['trees']['table']['group']  = t('Tree');
  $data['trees']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['trees']['bid'] = array(
    'title' => t('Top level trees'),
    'help' => t('The trees the node is in.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'bid',
      'handler' => 'views_handler_relationship',
      'label' => t('Tree'),
    ),
    // There is no argument here; if you need an argument, add the relationship
    // and use the node: nid argument.
  );

  // ----------------------------------------------------------------------
  // menu_links table -- this is aliased so we can get just trees relations

  // trees hierarchy and weight data are now in {menu_links}.
  $data['trees_menu_links']['table']['group'] = t('Tree');
  $data['trees_menu_links']['table']['join'] = array(
    'node' => array(
      'table' => 'menu_links',
      'left_table' => 'trees',
      'left_field' => 'mlid',
      'field' => 'mlid',
    ),
  );

  $data['trees_menu_links']['weight'] = array(
    'title' => t('Weight'),
    'help' => t('The weight of the trees page.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['trees_menu_links']['depth'] = array(
    'title' => t('Depth'),
    'help' => t('The depth of the trees page in the hierarchy; top level trees have a depth of 1.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );

  $data['trees_menu_links']['p'] = array(
    'title' => t('Hierarchy'),
    'help' => t('The order of pages in the trees hierarchy. If you want the exactly right order, remember to sort by weight, too.'),
    'sort' => array(
      'handler' => 'views_handler_sort_menu_hierarchy',
    ),
  );

  // ----------------------------------------------------------------------
  // trees_parent table -- this is an alias of the trees table which
  // represents the parent trees.

  // The {trees} record for the parent node.
  $data['trees_parent']['table']['group'] = t('Tree');
  $data['trees_parent']['table']['join'] = array(
    'node' => array(
      'table' => 'trees',
      'left_table' => 'trees_menu_links',
      'left_field' => 'plid',
      'field' => 'mlid',
    ),
  );

  $data['trees_parent']['nid'] = array(
    'title' => t('Parent'),
    'help' => t('The parent trees node.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Tree parent'),
    ),
  );

  return $data;
}

/**
 * @}
 */
