<?php
/**
 * @file
 * All functionallity for showing correct children, parents, siblings blocks.
 * 
 * @date
 * Jun 24, 2009
 */
 
/**
 * Get all the siblings from a node in a given trees.
 */
function get_trees_by_id($bid, $node) {
  
  static $trees = array();
          
  if(isset($trees[$node->nid][$bid])) {
    return $trees[$node->nid][$bid];
  }

  $result = db_query('SELECT * FROM {trees} b INNER JOIN {menu_links} ml ON b.mlid = ml.mlid WHERE b.nid = %d AND b.bid = %d', $node->nid, $bid);
  $tree = array();
  while($tree = db_fetch_array($result)) {
    $trees[$node->nid][$tree['bid']] = $tree;
  }            
  
  return $trees[$node->nid][$bid]; 
    
}

/**
 * Get the parents from a node in a given trees.
 */
function get_node_parents_by_trees($bid, $node, $page = 0) {

  $trees = get_trees_by_id($bid, $node);

  $parents = trees_parents($trees, FALSE);
  get_nodes_from_trees_elements($parents);
  
  $title = _trees_get_root_name($bid);
  if($title) {
    $block['title'] = t('@title - Current parents', array('@title' => $title));  
  }

  $content = theme('trees_parents', $parents);  
  $block['content'] = $content;
  return $block;
  
}

/**
 * Get the parents from a node in a given trees.
 */
function get_node_children_by_trees($bid, $node, $page = 0) {

  $trees = get_trees_by_id($bid, $node);
  $children = trees_children($trees, FALSE);
  get_nodes_from_trees_elements($children);

  $paging = array_chunk($children, variable_get('trees_block_children', 5));
  $children = $paging[$page];

  $title = _trees_get_root_name($bid);
  if($title) {
    $block['title'] = t('@title - Current children', array('@title' => $title));  
  } 

  $content = theme('trees_leaves', $children);  

  $content .= _get_trees_block_pager($paging, $page, 'children', $bid, $node);
  
  $block['content'] = $content;
  
  return $block;
  
}

/**
 * Get the siblings from a node in a given trees.
 */
function get_node_siblings_by_trees($bid, $node, $page = 0) {

  $trees = get_trees_by_id($bid, $node);
  $siblings = trees_siblings($trees, FALSE);
  get_nodes_from_trees_elements($siblings);  
  $paging = array_chunk($siblings, variable_get('trees_block_siblings', 5));
  $siblings = $paging[$page];
  
  $title = _trees_get_root_name($bid);
  if($title) {
    $block['title'] = t('@title - Current siblings', array('@title' => $title));  
  }
  
  $content = theme('trees_leaves', $siblings);
  $content .= _get_trees_block_pager($paging, $page, 'siblings', $bid, $node);
  $block['content'] = $content;
  return $block;
  
}

/**
 * Get the trees menu trees for a page, and return it as a linear array.
 *
 * @param $trees_link
 *   A fully loaded menu link that is part of the trees hierarchy.
 * @return
 *   A linear array of menu links in the order that the links are shown in the
 *   menu, so the previous and next pages are the elements before and after the
 *   element corresponding to $node.  The children of $node (if any) will come
 *   immediately after it in the array.
 */
function trees_get_flat_menu($trees_link) {
  static $flat = array();

  if (!isset($flat[$trees_link['mlid']])) {
    // Call menu_tree_all_data() to take advantage of the menu system's caching.
    $trees = menu_tree_all_data($trees_link['menu_name'], $trees_link);
    $flat[$trees_link['mlid']] = array();
    _trees_flatten_menu($trees, $flat[$trees_link['mlid']]);
  }
  return $flat[$trees_link['mlid']];
}

/**
 * Recursive helper function for trees_get_flat_menu().
 */
function _trees_flatten_menu($trees, &$flat) {
  foreach ($trees as $data) {
    if (!$data['link']['hidden']) {
      $flat[$data['link']['mlid']] = $data['link'];
      if ($data['below']) {
        _trees_flatten_menu($data['below'], $flat);
      }
    }
  }
}

/**
 * Format the menu links for the child pages of the current page.
 * @param $themed: return array if false, else return output
 */
function trees_children($trees_link, $themed = TRUE) {
  
  $flat = trees_get_flat_menu($trees_link);

  $children = array();

  if ($trees_link['has_children']) {
    // Walk through the array until we find the current page.
    do {
      $link = array_shift($flat);
    } while ($link && ($link['mlid'] != $trees_link['mlid']));
    // Continue though the array and collect the links whose parent is this page.
    while (($link = array_shift($flat)) && $link['plid'] == $trees_link['mlid']) {
      $data['link'] = $link;
      $data['below'] = '';
      $children[] = $data;
    }
  }
  
  if($themed) {
    return $children ? menu_trees_output($children) : '';  
  }
  
  return $children;
  
}

/**
 * Format the menu links for the child pages of the current page.
 * @param $themed: return array if false, else return output
 */
function trees_siblings($trees_link, $themed = TRUE) {
  
  $flat = trees_get_flat_menu($trees_link);
  $siblings = array();

  if ($trees_link['plid'] > 0) {    
    // Walk through the array until we find the parent page.
    do {
      $link = array_shift($flat);
    } while ($link && ($link['mlid'] != $trees_link['plid']));

    // Continue though the array and collect the links whose parent is same as page parent.
    while (($link = array_shift($flat))) {
      if($link['plid'] == $trees_link['plid']) {
        $data['link'] = $link;
        $data['below'] = '';
        $siblings[] = $data;
      }
    }

  }

  if($themed) {
    return $siblings ? menu_trees_output($siblings) : '';  
  }
  
  return $siblings;
  
}

/**
 * Format the menu links for the parent pages of the current page.
 * @param $themed: return array if false, else return output
 */
function trees_parents($trees_link, $themed = TRUE) {

  $flat = trees_get_flat_menu($trees_link);

  $parents = array();
  for($i = 1; $i <= 9; $i++) {
    if($trees_link['p'. $i] > 0 && $trees_link['mlid'] != $trees_link['p'. $i]) {
      $data['link'] = $flat[$trees_link['p'. $i]];
      $data['below'] = '';
      $parents[] = $data;  
    }    
  }
  
  if($themed) {
    return $parents ? menu_trees_output($parents) : '';  
  }
  
  return $parents;
  
}

/**
* Get all the nodes from a given array of trees elements and put them in the element.
*/
function get_nodes_from_trees_elements(&$elements) {
  
  foreach($elements as $key => $element) {
    $path = drupal_get_normal_path($element['link']['link_path']);  
    $nid = str_replace('node/', '', $path);
    if (is_numeric($nid)) {
      $elements[$key]['node'] = node_load($nid);
    }
  }
    
}

/**
 * Get the pager for a given array of results.
 */
function _get_trees_block_pager($paging, $page, $block, $bid, $node) {
  global $base_url;
  
  $pagers = array();
  $query = array(
    'block' => $block,
    'page' => $page - 1
  );
  $attributes = array('class' => 'trees-pager', 'rel' => $bid);  
  
  // show previous link if needed
  if($page > 0) {
    $pagers[] = l(t('Previous'), 'node/'. $node->nid, array('query' => $query, 'attributes' => $attributes));  
  }    
  
  $query['page'] = $page + 1;
  // show next link if needed
  if(count($paging) > $page + 1) {
    $pagers[] = l(t('Next'), 'node/'. $node->nid, array('query' => $query, 'attributes' => $attributes));
  }  
  
  $output = '';
  if(count($pagers) > 0) {
    $output = theme('item_list', $pagers, NULL, 'ul', array('class' => 'trees-pagers'));
  }
  
  return $output;
}

/**
* Ajax callback to update the correct trees content.
*/
function trees_ajax_update() {
  if(isset($_GET['block'])) {
    print $_GET['path'];
    if(!isset($_GET['nid'])) {
      exit();
    }
    
    $nid = $_GET['nid'];

    if (!is_numeric($nid)) {
      exit();
    }
    
    if(!isset($_GET['pid'])) {
      exit();
    }    
    $pid = $_GET['pid'];
    
    $node = node_load($nid);
    $page = 0;
    if(!isset($_GET['block']) && !isset($_GET['page'])) {
      exit();   
    }
    
    $page = $_GET['page'];    
        
    switch($_GET['block']) {
      
      case 'children':        
        $block = get_node_children_by_trees($pid, $node, $page);
      break;
      
      case 'siblings':
        $block = get_node_siblings_by_trees($pid, $node, $page);
      break;
      
    }
    
    $result = new stdClass();
    $result->result = TRUE;
    $result->data = $block['content'];
    $content = drupal_json($result);
    
  }
  
  exit();
}