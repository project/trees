<?php

/**
 * @file
 * User page callbacks for the trees module.
 */

/**
 * Menu callback; prints a listing of all trees.
 */
function trees_render() {
  $trees_list = array();
  foreach (trees_get_trees() as $tree) {
    $trees_list[] = l($tree['title'], $tree['href'], $tree['options']);
  }

  return theme('item_list', $trees_list);
}

/**
 * Menu callback; show the outline form for a single node.
 */
function trees_outline($node) {
  drupal_set_title(check_plain($node->title));
  //hier eventueel een new toevoegen zoals in de prepare van de hook nodeapi
  if (!empty($node->trees)) {
    foreach ($node->trees as $bid => $trees) {
      $output .= drupal_get_form('trees_outline_form', $node, $bid);
    }
  }
  else {
    $output = '<form>'.t('This node  is not part of any trees yet, proceed to the !link', array('!link' => l(t('node edit form'), 'node/'.$node->nid.'/edit'))).'</form>';
  }

  return $output;
}

/**
 * Build the form to handle all trees outline operations via the outline tab.
 *
 * @see trees_outline_form_submit()
 * @see trees_remove_button_submit()
 *
 * @ingroup forms
 */
function trees_outline_form(&$form_state, $node, $bid) {
  if (!isset($node->trees[$bid])) {
    // The node is not part of any trees yet - set default options.
    $node->trees[$bid] = _trees_link_defaults($node->nid);
  }
  else {
    $node->trees[$bid]['original_bid'] = $node->trees[$bid]['bid'];
  }
  // Find the depth limit for the parent select.
  if (!isset($node->trees[$bid]['parent_depth_limit'])) {
    $node->trees[$bid]['parent_depth_limit'] = _trees_parent_depth_limit($node->trees[$bid]);
  }
  $form['#node'] = $node;
  $form['#id'] = 'trees-outline-'.$bid;

  _trees_add_form_elements($form, $node, $bid);

  $form['trees'][$bid]['#collapsible'] = FALSE;

  $form['trees'][$bid]['remove'] = array(
    '#type' => 'submit',
    '#value' => t('Remove from trees outline'),
    '#access' => $node->nid != $node->trees[$bid]['bid'] && $node->trees[$bid]['bid'],
    '#weight' => 20,
    '#submit' => array('trees_remove_button_submit'),
  );

  $form['trees_bid'] = array(
    '#value' => $bid,
    '#access' => false
  );
  $form['trees'][$bid]['update'] = array(
    '#type' => 'submit',
    '#value' => t('Save trees outline'),
    '#weight' => 15,
  );

  return $form;
}

/**
 * Button submit function to redirect to removal confirm form.
 *
 * @see trees_outline_form()
 */
function trees_remove_button_submit($form, &$form_state) {
  $form_state['redirect'] = 'node/'. $form['#node']->nid .'/outline/remove/'.$form['trees_bid']['#value'];
}



/**
 * Handles trees outline form submissions from the outline tab.
 *
 * @see trees_outline_form()
 */
function trees_outline_form_submit($form, &$form_state) {
  $node = $form['#node'];
  $form_state['redirect'] = "node/". $node->nid."/outline";
  $trees_links = $form_state['values']['trees'];

  foreach ($trees_links as $bid => $trees_link) {
    if (!$trees_link['bid']) {
      drupal_set_message(t('No changes were made'));
      return;
    }

    $trees_link['menu_name'] = trees_menu_name($trees_link['bid']);
    $node->trees = $trees_link;
    if (_trees_update_outline($node)) {
      if ($node->trees['parent_mismatch']) {
        // This will usually only happen when JS is disabled.
        drupal_set_message(t('The post has been added to the selected trees. You may now position it relative to other pages.'));
        $form_state['redirect'] = "node/". $node->nid ."/outline";
      }
      else {
        drupal_set_message(t('The trees outline has been updated.'));
      }
    }
    else {
      drupal_set_message(t('There was an error adding the post to the trees.'), 'error');
    }
  }
}

/**
 * Menu callback; builds a form to confirm removal of a node from the trees.
 *
 * @see trees_remove_form_submit()
 *
 * @ingroup forms
 */
function trees_remove_form(&$form_state, $node, $bid) {
  $form['#node'] = $node;
  $form['trees_bid'] = array(
    '#value' => $bid,
    '#access' => false
  );
  $title = array('%title' => $node->title);

  if ($node->trees['has_children']) {
    $description = t('%title has associated child pages, which will be relocated automatically to maintain their connection to the trees. To recreate the hierarchy (as it was before removing this page), %title may be added again by editing the node, and each of its former child pages will need to be relocated manually.', $title);
  }
  else {
    $description = t('%title may be added to hierarchy again by editing the node.', $title);
  }

  return confirm_form($form, t('Are you sure you want to remove %title from the trees hierarchy?', $title), 'node/'. $node->nid, $description, t('Remove'));
}

/**
 * Confirm form submit function to remove a node from the trees.
 *
 * @see trees_remove_form()
 */
function trees_remove_form_submit($form, &$form_state) {
  $node = $form['#node'];

  if ($form['trees_bid']['#value']) {
    $trees[$form['trees_bid']['#value']] = $form['#node']->trees[$form['trees_bid']['#value']];
  }
  else {
    $trees = $form['#node']->trees;
  }

  foreach ($trees as $tree) {
    if ($node->nid != $node->trees[$form['trees_bid']['#value']]['bid']) {
      // Only allowed when this is not a trees (top-level page).
      menu_link_delete($node->trees[$form['trees_bid']['#value']]['mlid']);
      dsm($tree['mlid']);
      db_query('DELETE FROM {trees} WHERE mlid = %d', $tree['mlid']);
      drupal_set_message(t('The post has been removed from the trees.'));
    }
    else {
      $menu_name = db_query("SELECT menu_name FROM menu_links WHERE mlid = %d", $tree['mlid']);
      db_query('DELETE FROM {trees} WHERE mlid = %d', $tree['mlid']);
      menu_link_delete($node->trees[$form['trees_bid']['#value']]['mlid']);
      drupal_set_message(t('The post has been removed from the trees.'));
      menu_cache_clear();
      _menu_clear_page_cache();
    }
    $form_state['redirect'] = 'node/' . $node->nid . '/outline';
  }
}