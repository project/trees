<?php
/**
 * @file
 * All theming functions for frontend output.
 * 
 * @date
 * Jun 24, 2009
 */

/**
 * Return a themed list of leaves.
 */
function theme_trees_leaves($leaves) {

  if(count($leaves) == 0) {
    return '';
  }

  $items = array();
  foreach($leaves as $leave) {
    $items[] =  theme('trees_leave', $leave);
  }

  return theme('item_list', $items);
  
}

/**
 * Return a themed list of parents.
 */
function theme_trees_parents($parents) {
  
  if(count($parents) == 0) {
    return '';
  }

  $output = '<ul class="trees-parents trees-leaves">';
  foreach($parents as $key => $parent) {
    if($key > 0) {
      $output .= '<ul>';
    }
    $output .= '<li>'. theme('trees_leave', $parent);
    
  }
  
  foreach($parents as $key => $parent) {
    $output .= '</li></ul>';    
  }   
  
  return $output;
  
}

/**
* Return a themed leave.
*/
function theme_trees_leave($leave) {

  if(isset($leave['node'])) {
    $node = $leave['node'];
    $node->build_mode = 'block';
    $output = node_view($node, FALSE, FALSE, FALSE);    
  }
  else {
    $output = l($leave['link']['title'], $leave['link']['link_path']);
  }
  
  return $output;
  
}