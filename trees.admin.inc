<?php

/**
 * @file
 * Admin page callbacks for the trees module.
 */

/**
 * Returns an administrative overview of all trees.
 */
function trees_admin_overview() {
  $rows = array();

  foreach (trees_get_trees() as $tree) {
    $rows[] = array(l($tree['title'], $tree['href'], $tree['options']), l(t('edit order and titles'), "admin/content/trees/". $tree['nid']), l('remove trees', 'node/'. $tree['nid'] .'/outline/remove/'. $tree['bid'], array('query' => drupal_get_destination())));
  }
  $headers = array(t('trees'), array('data' => t('Operations'), 'colspan' => '2'));

  return theme('table', $headers, $rows);
}

/**
 * Builds and returns the trees settings form.
 *
 * @see trees_admin_settings_validate()
 *
 * @ingroup forms
 */
function trees_admin_settings() {
  $types = node_get_types('names');
  $form['trees_allowed_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allowed trees outline types'),
    '#default_value' => variable_get('trees_allowed_types', array('trees')),
    '#options' => $types,
    '#description' => t('Select content types which users with the %add-perm permission will be allowed to add to the trees hierarchy. Users with the %outline-perm permission can add all content types.', array('%add-perm' => t('add content to trees'),  '%outline-perm' => t('administer trees outlines'))),
    '#required' => TRUE,
  );
  $form['trees_child_type'] = array(
    '#type' => 'radios',
    '#title' => t('Default child page type'),
    '#default_value' => variable_get('trees_child_type', 'trees'),
    '#options' => $types,
    '#description' => t('The content type for the %add-child link must be one of those selected as an allowed trees outline type.', array('%add-child' => t('Add child page'))),
    '#required' => TRUE,
  );
  $form['trees_block_siblings'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of siblings in sibling block'),
    '#default_value' => variable_get('trees_block_siblings', 5),
  );
  $form['trees_block_children'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of siblings in sibling block'),
    '#default_value' => variable_get('trees_block_children', 5),
  ); 
  
  $form['array_filter'] = array('#type' => 'value', '#value' => TRUE);
  $form['#validate'][] = 'trees_admin_settings_validate';
  return system_settings_form($form);
}

/**
 * Validate the trees settings form.
 *
 * @see trees_admin_settings()
 */
function trees_admin_settings_validate($form, &$form_state) {
  $child_type = $form_state['values']['trees_child_type'];
  if (empty($form_state['values']['trees_allowed_types'][$child_type])) {
    form_set_error('trees_child_type', t('The content type for the %add-child link must be one of those selected as an allowed trees outline type.', array('%add-child' => t('Add child page'))));
  }
}

/**
 * Build the form to administrate the hierarchy of a single trees.
 *
 * @see trees_admin_edit_submit()
 *
 * @ingroup forms.
 */
function trees_admin_edit($form_state, $node) {
  drupal_set_title(check_plain($node->title));
  $form = array();
  $form['#node'] = $node;
  _trees_admin_table($node, $form);
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save trees pages'),
  );
  return $form;
}

/**
 * Check that the trees has not been changed while using the form.
 *
 * @see trees_admin_edit()
 */
function trees_admin_edit_validate($form, &$form_state) {
  if ($form_state['values']['trees_hash'] != $form_state['values']['trees_current_hash']) {
    form_set_error('', t('This trees has been modified by another user, the changes could not be saved.'));
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Handle submission of the trees administrative page form.
 *
 * This function takes care to save parent menu items before their children.
 * Saving menu items in the incorrect order can break the menu trees.
 *
 * @see trees_admin_edit()
 * @see menu_overview_form_submit()
 */
function trees_admin_edit_submit($form, &$form_state) {
  // Save elements in the same order as defined in post rather than the form.
  // This ensures parents are updated before their children, preventing orphans.
  $order = array_flip(array_keys($form['#post']['table']));
  $form['table'] = array_merge($order, $form['table']);

  foreach (element_children($form['table']) as $key) {
    if ($form['table'][$key]['#item']) {
      $row = $form['table'][$key];
      $values = $form_state['values']['table'][$key];

      // Update menu item if moved.
      if ($row['plid']['#default_value'] != $values['plid'] || $row['weight']['#default_value'] != $values['weight']) {
        $row['#item']['plid'] = $values['plid'];
        $row['#item']['weight'] = $values['weight'];
        menu_link_save($row['#item']);
      }

      // Update the title if changed.
      if ($row['title']['#default_value'] != $values['title']) {
        $node = node_load($values['nid'], FALSE);
        $node->title = $values['title'];
        $node->trees['link_title'] = $values['title'];
        $node->revision = 1;
        $node->log = t('Title changed from %original to %current.', array('%original' => $node->title, '%current' => $values['title']));
        node_save($node);
        watchdog('content', 'trees: updated %title.', array('%title' => $node->title), WATCHDOG_NOTICE, l(t('view'), 'node/'. $node->nid));
      }
    }
  }

  drupal_set_message(t('Updated trees %title.', array('%title' => $form['#node']->title)));
}

/**
 * Build the table portion of the form for the trees administration page.
 *
 * @see trees_admin_edit()
 */
function _trees_admin_table($node, &$form) {
  $form['table'] = array(
    '#theme' => 'trees_admin_table',
    '#tree' => TRUE,
  );

  foreach(_trees_get_treesbids() as $bid) {
    $trees = trees_menu_subtrees_data($node->trees[$bid]);

    $trees = array_shift($trees); // Do not include the trees item itself.
    if ($trees['below']) {
      $hash = sha1(serialize($trees['below']));
      // Store the hash value as a hidden form element so that we can detect
      // if another user changed the trees hierarchy.
      $form['trees_hash'] = array(
        '#type' => 'hidden',
        '#default_value' => $hash,
      );
      $form['trees_current_hash'] = array(
        '#type' => 'value',
        '#value' => $hash,
      );
      _trees_admin_table_trees($trees['below'], $form['table']);
    }
  }
}

/**
 * Recursive helper to build the main table in the trees administration page form.
 *
 * @see trees_admin_edit()
 */
function _trees_admin_table_trees($trees, &$form) {
  foreach ($trees as $data) {

    $type = db_result(db_query('SELECT type FROM {node} WHERE nid = %d', $data['link']['nid']));

    $form['trees-admin-'. $data['link']['nid']] = array(
      '#item' => $data['link'],
      'type' => array('#type' => 'hidden', '#value' => $type),
      'nid' => array('#type' => 'value', '#value' => $data['link']['nid']),
      'depth' => array('#type' => 'hidden', '#value' => $data['link']['depth']),
      'href' => array('#type' => 'value', '#value' => $data['link']['href']),
      'title' => array('#type' => 'value', '#value' => $data['link']['link_title']),
      'weight' => array(
        '#type' => 'weight',
        '#default_value' => $data['link']['weight'],
        '#delta' => 15,
      ),
      'plid' => array(
        '#type' => 'textfield',
        '#default_value' => $data['link']['plid'],
        '#size' => 6,
      ),
      'mlid' => array(
        '#type' => 'hidden',
        '#default_value' => $data['link']['mlid'],
      ),
    );
    if ($data['below']) {
      _trees_admin_table_trees($data['below'], $form);
    }
  }

  return $form;
}

/**
 * Theme function for the trees administration page form.
 *
 * @ingroup themeable
 * @see trees_admin_table()
 */
function theme_trees_admin_table($form) {

  drupal_add_tabledrag('trees-outline', 'match', 'parent', 'trees-plid', 'trees-plid', 'trees-mlid', TRUE, MENU_MAX_DEPTH - 2);
  drupal_add_tabledrag('trees-outline', 'order', 'sibling', 'trees-weight');

  $header = array(t('Title'), t('Weight'), t('Parent'), array('data' => t('Operations'), 'colspan' => '4'));

  $rows = array();
  $destination = drupal_get_destination();
  $access = user_access('administer nodes');
  foreach (element_children($form) as $key) {
    $nid = $form[$key]['nid']['#value'];
    $href = $form[$key]['href']['#value'];

    // Add special classes to be used with tabledrag.js.
    $form[$key]['plid']['#attributes']['class'] = 'trees-plid';
    $form[$key]['mlid']['#attributes']['class'] = 'trees-mlid';
    $form[$key]['weight']['#attributes']['class'] = 'trees-weight';

    $title = theme('indentation', $form[$key]['depth']['#value'] - 2) . $form[$key]['title']['#value'];
    if(isset($form[$key]['children'])) {
      $title .= ' ('. $form[$key]['children']['#value'] .')';
    }

    $data = array(
      $title,
      drupal_render($form[$key]['weight']).drupal_render($form[$key]['depth']).drupal_render($form[$key]['type']),
      drupal_render($form[$key]['plid']) . drupal_render($form[$key]['mlid']),
      l(t('view'), $href),
      $access ? l(t('edit'), 'node/'. $nid .'/edit', array('query' => $destination)) : '&nbsp',
      $access ? l(t('delete'), 'node/'. $nid .'/delete', array('query' => $destination) )  : '&nbsp',
      $access ? l(t('remove from trees'), 'node/'. $nid .'/outline/remove/'. arg(3), array('query' => $destination) )  : '&nbsp',
    );
    $row = array('data' => $data);
    if (isset($form[$key]['#attributes'])) {
      $row = array_merge($row, $form[$key]['#attributes']);
    }
    $row['class'] = empty($row['class']) ? 'draggable' : $row['class'] .' draggable';
    $rows[] = $row;
  }

  return theme('table', $header, $rows, array('id' => 'trees-outline'));
}