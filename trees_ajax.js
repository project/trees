Drupal.Tree = Drupal.Tree || {};
Drupal.Tree.Ajax = Drupal.Tree.Ajax || {};

/**
* Hook ajax paging on all the trees pagers.
*/
Drupal.Tree.Ajax.bindPagers = function(target) {  
  $(target).find('a.trees-pager').click(function() { return Drupal.Tree.Ajax.pager(this, target); });   
}

/**
 * Process the paging request.
 */
Drupal.Tree.Ajax.pager = function(link, target) { 
   
  var ajax_path = Drupal.settings.trees.ajax_path;
  var href = $(link).attr('href');
  
  var data = Drupal.Tree.Ajax.getUrlVars(href);
  data += '&nid=' + node_nid;
  data += '&pid=' + $(link).attr('rel');

  $.ajax({
    url: ajax_path,
    type: 'GET',
    data: data,
    success: function(response) {
      target.innerHTML = response.data;
      Drupal.Tree.Ajax.bindPagers(target);
    },
    error: function() { alert(Drupal.t("An error occurred at @path.", {'@path': ajax_path})); },
    dataType: 'json'
  });
  
  return false;
  
}

/**
 * Get the path from a given url.
 */
Drupal.Tree.Ajax.getUrl = function (url) {
   return url.slice(0, url.indexOf('?'));
}

/**
 * Get the url vars from a given url.
 */
Drupal.Tree.Ajax.getUrlVars = function (url) {
   return url.slice(url.indexOf('?') + 1);
}

/**
 * Search all divs with possible pagers.
 */
$(document).ready(function() {
  $('div.trees-content-holder').each(function() {
    Drupal.Tree.Ajax.bindPagers(this);
  });
});